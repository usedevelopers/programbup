namespace BringUp {

    @component('date-bup')
    export class DateBup extends NowElements.BaseView {
		/**
		 * Boolean for controlling the spinner
		 * @type {Boolean}
		 */
        @property({
            type: Boolean,
            notify: true
        })
        mainLoading: boolean;
		/**
		 * The settings object
		 * @type {Object}
		 */
        @property({
            type: Object,
            observer: '_onSettings'
        })
        settings: any;
		/**
		 * The UNID of the document to load
		 * @type {String}
		 */
        @property({
            type: String,
            notify: true
        })
        selectedDoc: string;
		/**
		 * The route object
		 * @type {Object}
		 */
        @property({
            type: Object,
            notify: true
        })
        route: any;
		/**
		 * The callback for the vaadin-grid.items function
		 * @type {Function}
		 */
        @property({
            type: Function
        })
        _gridItemsCallback: any;
		/**
		 * Temporary property for determining if the size of the grid
		 * needs to be updated with the actual size of the entries
		 * @type {Number}
		 */
        @property({
            type: Number,
            value: 0
        })
        _gridSize: number;
		/**
		 * Listener for the vaadin-grid.selected-items-changed event. Navigates
		 * to the selected document
		 */
        @listen('dateBup.selected-items-changed')
        private _onRowSelected() {
            // console.log(this.is, '_onRowSelected', arguments);
            let grid = this.$.dateBup;
            let selectedIdx = grid.selection.selected();
            if (selectedIdx && selectedIdx.length > 0) {
                let that = this;
                grid.getItem(selectedIdx, function (err, item) {
                    that.set('route.path', '/doc/' + item['@unid']);
                });
            }
        }
		/**
		 * Get the URL to the view. Must wait for the settings object to be truthy
		 * @param {Object} settings The settings object
		 * @return {String}
		 */
        private _getUrl(settings) {
            var url = null;
            var prefix = settings.NSF_URL;
            //var mid = '/api/data/collections/name/By%20Bring%20Up%20Date';
            var mid = '/api/data/collections/unid/E566840EDBE8FCE08525747300554BC0';
            return prefix + mid;
        }
		/**
		 * Fired when the settings change. Sets up the grid along with infinite
		 * scrolling
		 * @param {Object} newVal The new settings value
		 * @param {Object} oldVal The old settings value
		 */
        private _onSettings(newVal, oldVal) {
            if (newVal) {
                let ajax = this.$.viewAjax;
                // Setup the grid
                let grid = this.$.dateBup;
                grid.size = 50;
                grid.visibleRows = 12;
                // this function is just magically ran when the grid is initialized
                grid.items = (params, callback) => {
                    this._gridItemsCallback = callback;
                    this._fetchRows(this.settings, params.index, params.count);
                }
                for (var i = 0; i < grid.columns.length; i++) {
					if (grid.columns[i].name.includes('date')) {
						//console.log('grid.columns[i].name', grid.columns[i].name);
						grid.columns[i].renderer = this._dateCellRenderer;
					}
				}
                grid.cellClassGenerator = this._gridCellClassGenerator;
            }
        }
		/**
		 * Fetch the rows of the grid
		 * @param {Object} settings The settings object
		 * @param {Number} start    The index to start the fetch at
		 * @param {Number} count    The number of rows to fetch
		 */
        private _fetchRows(settings, start, count) {
            var ajax = this.$.viewAjax;
            ajax.params = {
                start: start,
                count: count
            };
            ajax.generateRequest();
        }
		/**
		 * Fired once a response is received from fetching rows. Runs the
		 * vaadin-grid.items callback function to add the fetched items to the grid
		 * and update the size of the grid based on the first item's '@siblings'
		 * property
		 * @param {Event} evt    The event object
		 * @param {Object} detail The detail object
		 */
        private _onViewFetch(evt, detail) {
            let gridItems = evt.detail.response;
            let grid = this.$.dateBup;
            this._gridItemsCallback(gridItems);
            if (this._gridSize === 0 && gridItems.length > 0) {
                grid.size = gridItems[0]['@siblings'];
                this._gridSize = grid.size;
            }
        }
		/**
		 * Set's a style class on the last cell of each row
		 * @param {Object} cell The vaadin-grid.cell object
		 */
        private _gridCellClassGenerator(cell) {
            if (cell.index === 2) {
                return 'titleCell';
            }
        }
		/**
		 * Format date columns as MM/DD/YYYY
		 * @param {Object} cell The vaadin-grid.cell object
		 */
		private _dateCellRenderer(cell) {
			if (cell.data) {
				var val = moment(cell.data).format('MM/DD/YYYY');
				cell.element.textContent = val;
			}
		}
    }
}

BringUp.DateBup.register();